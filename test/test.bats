#!/usr/bin/env bats

# Test GCP app engine, https://cloud.google.com/appengine/

set -e

setup() {
  IMAGE_NAME=${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}
  RANDOM_NUMBER=$RANDOM
  sed s/{{BODY}}/${RANDOM_NUMBER}/g test/app.js.template >> test/app.js
  sed s/{{BODY}}/${RANDOM_NUMBER}/g test/package.json.template >> test/package.json
}

teardown() {
  rm test/app.js
  rm test/package.json
}

@test "upload to GCP app engine" {
  # execute tests
  run docker run \
    -e KEY_FILE="${GCP_KEY_FILE}" \
    -e PROJECT="${PROJECT}" \
    -e VERSION="${BITBUCKET_BUILD_NUMBER}" \
    -e PROMOTE="true" \
    -e STOP_PREVIOUS_VERSION="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd)/test \
    $IMAGE_NAME

  echo "${output}"

  [[ "${status}" == "0" ]]

  sleep 15

  # verify
  run curl --silent "https://service-0-dot-${PROJECT}.appspot.com"
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${RANDOM_NUMBER}" ]]
}

@test "upload to GCP app engine different deployable" {
  # execute tests
  run docker run \
    -e KEY_FILE="${GCP_KEY_FILE}" \
    -e PROJECT="${PROJECT}" \
    -e DEPLOYABLES="app-1.yaml" \
    -e VERSION="${BITBUCKET_BUILD_NUMBER}" \
    -e PROMOTE="true" \
    -e STOP_PREVIOUS_VERSION="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd)/test \
    $IMAGE_NAME

  echo "${output}"

  [[ "${status}" == "0" ]]

  sleep 15

  run curl --silent "https://service-1-dot-${PROJECT}.appspot.com"
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${RANDOM_NUMBER}" ]]
}

@test "upload to GCP app engine small timeout" {
  # execute tests
  run docker run \
    -e KEY_FILE="${GCP_KEY_FILE}" \
    -e PROJECT="${PROJECT}" \
    -e DEPLOYABLES="app-2.yaml" \
    -e VERSION="${BITBUCKET_BUILD_NUMBER}" \
    -e PROMOTE="true" \
    -e STOP_PREVIOUS_VERSION="true" \
    -e CLOUD_BUILD_TIMEOUT=1 \
    -v $(pwd):$(pwd) \
    -w $(pwd)/test \
    $IMAGE_NAME

  echo "${output}"

  [[ "${status}" != "0" ]]
  [[ "${output}" == *"DEADLINE_EXCEEDED"* ]]
}
